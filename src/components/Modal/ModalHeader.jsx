import styles from './Modal.module.css'

function ModalHeader({children}) {
     return (
          <div className={styles.modalHeader}>
               {children}
          </div>
     )
}
export default ModalHeader;