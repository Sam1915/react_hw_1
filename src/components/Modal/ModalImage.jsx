import Modal from './Modal';
import ModalHeader from './ModalHeader';
import ModalBody from './ModalBody';
import ModalFooter from './ModalFooter';
import ModalClose from './ModalClose';
import ModalWrapper from './ModalWrapper';
import styles from './Modal.module.css';

function ModalImage({ imageUrl, onClose }) {
  return (
    <ModalWrapper onClick={onClose}>
      <Modal onClose={onClose}>
        <div className={styles.headerWrapper}>
          <ModalHeader>Image Modal</ModalHeader>
          <ModalClose onClick={onClose} />
        </div>
        <ModalBody>
          <img className={styles.modalPic} src={imageUrl} alt="Modal" />
        </ModalBody>
        <ModalFooter firstText="Close" firstClick={onClose} />
      </Modal>
    </ModalWrapper>
  );
};

export default ModalImage;
