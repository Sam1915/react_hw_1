import styles from './Modal.module.css'

function ModalWrapper({ children, onClick }) {
     return (
          <div className={styles.modalWrapper} onClick={onClick}>
               {children}
          </div>
     )
}
export default ModalWrapper;