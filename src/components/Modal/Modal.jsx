import ModalWrapper from "./ModalWrapper"
import ModalHeader from "./ModalHeader"
import ModalFooter from "./ModalFooter"
import styles from './Modal.module.css'

function Modal({ className, children }) {
     return (
          <div className={`${styles.modal} ${styles[className]}`} onClick={e => e.stopPropagation()}>
               {children}
          </div>
     )
}
export default Modal;