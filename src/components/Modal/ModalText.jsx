import Modal from './Modal';
import ModalHeader from './ModalHeader';
import ModalBody from './ModalBody';
import ModalFooter from './ModalFooter';
import ModalClose from './ModalClose';
import ModalWrapper from './ModalWrapper';
import styles from './Modal.module.css';

function ModalText({ text, onClose }) {
  return (
    <ModalWrapper onClick={onClose}>
      <Modal className={'modalTwo'} onClose={onClose}>
        <div className={styles.headerWrapper}>
          <ModalHeader>Text Modal</ModalHeader>
          <ModalClose onClick={onClose} />
        </div>
        <ModalBody>
          <p>{text}</p>
        </ModalBody>
        <ModalFooter firstText="Close" firstClick={onClose} />
      </Modal>
    </ModalWrapper>
  );
};

export default ModalText;
