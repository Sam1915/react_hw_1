import styles from './Modal.module.css';

function ModalBody({children}) {
  return (
    <div className={styles.modalBody}>
      {children}
    </div>
  );
};

export default ModalBody;