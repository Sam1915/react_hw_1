import styles from './Modal.module.css'

function ModalClose({ onClick }) {
   return (
      <div className={styles.modalClose} onClick={onClick}>
         &times;
      </div>
   );
};

export default ModalClose;
