import React from 'react';
import { useState } from 'react';
import Button from './components/Button/Button';
import ModalImage from './components/Modal/ModalImage';
import ModalText from './components/Modal/ModalText';
import modalBG from '../public/image/modalBG.jpg';
import styles from './components/Modal/Modal.module.css';

function App() {
   const [openFirstModal, setOpenFirstModal] = useState(false);
   const [openSecondModal, setOpenSecondModal] = useState(false);

   const handleFirstModal = () => {
      setOpenFirstModal((prev) => !prev)
   }
   const handleSecondModal = () => {
      setOpenSecondModal((prev) => !prev)
   }

   return (
      <>
         <Button className={styles.firstButton} onClick={handleFirstModal}>
            Open first modal 
         </Button>
         <Button className={styles.secondButton} onClick={handleSecondModal}>
            Open second modal
         </Button>
         {openFirstModal && <ModalImage imageUrl={modalBG} onClose={handleFirstModal}></ModalImage>}
         {openSecondModal && <ModalText onClose={handleSecondModal}></ModalText>}
      </>
   );
};

export default App;